﻿using System;

namespace String_Calculator_Version8Timed
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }
            var total = GetTotal(numbers);
            
            return total;
        }

        public int GetTotal(string numbers)
        {
            var sum = 0;

            foreach (var number in numbers.Split(',','\n'))
            {
                sum += int.Parse(number);
            }

            return sum;
        }
    }
}
