﻿using NUnit.Framework;
using String_Calculator_Version8Timed;

namespace StringCalculatorTest
{
    [TestFixture]
    public class StringCalculatorTest
    {
        private StringCalculator _calculator;

        [SetUp]
        public void SetUp()
        {
            _calculator = new StringCalculator();
        }

        [Test]
        public void GivenEmptyString_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 0;

            //Act
            var actual = _calculator.Add("");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenOneNumber_WhenAdding_ReturnSum()
        {
            //Arrange
            var expected = 1;

            //Act
            var actual = _calculator.Add("1");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenTwoNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("1,2");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GiveUnlimitedNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("1,2,2,1");

            //Assert
            Assert.AreEqual(expected,actual);
        }
            
        [Test]
        public void GivenNewLine_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 6;

            //Act
            var actual = _calculator.Add("1\n2,3");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenCostumDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("//;\n1;2");

            //Assert
            Assert.AreEqual(expected,actual);
        }
    }
}
